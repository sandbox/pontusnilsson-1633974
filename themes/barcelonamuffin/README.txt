
*************
* Färgstuff *
*************

BRONS LJUS | #9C6114 (brons enl. befintlig grafisk profil, på plattor och linjer) 
BRONS MÖRK | #8E5614 (på rubriker & texter)
BLÅ | #000080 ( i logotype och i länkar i brödtext)

KOMPLETTERANDE FÄRGER

LJUSROSA | #F8D0E3 (Puffar och tonplattor)
HIMMELSBLÅ | #B8E0F1 (Puffar och tonplattor)
KOPPARGRÖN | #C4E2D6 (puffar och tonplattor samt sidtopp och sidfot)
BEIGE | #F2EAD9 (bakgrund på textsidor och i bodyn för att skilja ut textblock)
MÖRK BEIGE | #D2BA81 (som markering i primära navigeringen att en menypunkt är vald samt färg på vald vänsternavigeringspunkt)
GRÅ | #303030 (använd i brödtexter och puffar)

HIGHLIGHTS

GUL | #F3EB04 (på hänvisningar såsom LÄS MER...och på mediaikoner PLAY för att indikera film)
CERISE | #E01D78 (på hänvisningar såsom LÄS MER och underrubriker...och på mediaikoner )