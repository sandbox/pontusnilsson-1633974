<?php
/**
 * @file
 * bc_settings.features.language.inc
 */

/**
 * Implements hook_locale_default_languages().
 */
function bc_settings_locale_default_languages() {
  $languages = array();

  // Exported language: en
  $languages['en'] = array(
    'language' => 'en',
    'name' => 'English',
    'native' => 'English',
    'direction' => '0',
    'enabled' => '1',
    'plurals' => '0',
    'formula' => '',
    'domain' => '',
    'prefix' => '',
    'weight' => '0',
  );
  // Exported language: es
  $languages['es'] = array(
    'language' => 'es',
    'name' => 'Spanish',
    'native' => 'Español',
    'direction' => '0',
    'enabled' => '1',
    'plurals' => '0',
    'formula' => '',
    'domain' => '',
    'prefix' => 'es',
    'weight' => '0',
  );
  // Exported language: sv
  $languages['sv'] = array(
    'language' => 'sv',
    'name' => 'Swedish',
    'native' => 'Svenska',
    'direction' => '0',
    'enabled' => '1',
    'plurals' => '0',
    'formula' => '',
    'domain' => '',
    'prefix' => 'sv',
    'weight' => '0',
  );
  return $languages;
}
