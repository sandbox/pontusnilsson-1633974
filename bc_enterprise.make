api = 2

core = 7.x

projects[transliteration][version] = 3.0
projects[transliteration][subdir] = contrib
projects[pathauto][version] = 1.1
projects[pathauto][subdir] = contrib
projects[simpletest_clone][version] = 1.0-beta3
projects[simpletest_clone][subdir] = contrib
projects[metatag][version] = 1.0-alpha6
projects[metatag][subdir] = contrib
projects[scheduler][version] = 1.0
projects[scheduler][subdir] = contrib
projects[features_override][version] = 2.0-alpha1
projects[features_override][subdir] = contrib
projects[drupal_ipsum][version] = 1.7
projects[drupal_ipsum][subdir] = contrib

; The enterprise product
projects[ns_prod_enterprise][type] = module
projects[ns_prod_enterprise][version] = 2.x
projects[ns_prod_enterprise][download][type] = git
projects[ns_prod_enterprise][download][url] = http://git.drupal.org/project/ns_prod_enterprise.git
projects[ns_prod_enterprise][download][branch] = master
projects[ns_prod_enterprise][subdir] = contrib

; Fetch file_lock has no stable release.
projects[file_lock][type] = module
projects[file_lock][version] = 1.x
projects[file_lock][download][type] = git
projects[file_lock][download][revision] = a84c0ed
projects[file_lock][subdir] = contrib

; Fetch the current version of NS Core.
projects[ns_core][type] = module
projects[ns_core][version] = 2.x
projects[ns_core][download][type] = git
projects[ns_core][download][branch] = 7.x-2.x
projects[ns_core][subdir] = contrib
;projects[ns_core][download][revision] = e3d9631735e4a8ff5efade67bbb83cec1839ee34
